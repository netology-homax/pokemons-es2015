class Pokemon {
    constructor(name = 'noname', level = 1) {
        this.name = typeof name === 'string' && name || 'villian';
        this.level = typeof level === 'number' && level || 1;
    }

    show() {
        console.log(`Name: ${this.name}, Level: ${this.level}`);
    }

    valueOf() {
        return this.level;
    }

    toString() {
        return `${this.name}(${this.level})`;
    }
}

class PokemonList extends Array {
    constructor(...pokemons) {
        super(...pokemons.filter(pokemon => pokemon instanceof Pokemon));
    }
    add(name, level) {
        this.push(new Pokemon(name, level));
    }
    show() {
        for (let pokemon of this) {
            pokemon.show();
        }
        console.log('____');
        console.log(`Total count: ${this.length}`);
    }
    max() {
        let maxLevel = Math.max(...this);
        return this.find(pokemon => pokemon.level === maxLevel);
    }
}

pok1 = new Pokemon("Chupa", 3);
pok2 = new Pokemon("Chupa");
pok3 = new Pokemon("Chupa", null);
pok4 = new Pokemon();
pok5 = new Pokemon(null);
pok6 = new Pokemon(undefined);
pok7 = {name: 'figlik', level: 100};

//Набираем армию потерянных
lost = new PokemonList(pok1, pok2, pok3);
lost.add('Baltazavr', 6);
lost.add('Mikki', 6);
lost.add('Nigma', 4);
lost.add('Virchu', 6);

//Набираем армию найденных
found = new PokemonList(pok4, pok5, pok6, pok7);
found.add('Pikachu', 11);
found.add('Fox', 7);
found.add({test: 123}, [7, 6, 7, 5, 4]);

//Армия найденных вербует отставшего из потерянных
found.push(lost.pop());

console.log(`Lost pokemon list:\n`);
lost.show();
console.log('------------------------');
console.log(`Found pokemon list:\n`);
found.show();

console.log('------------------------');
console.log(`Max of lost: ${lost.max()}`);
console.log(`Max of found: ${found.max()}`);
